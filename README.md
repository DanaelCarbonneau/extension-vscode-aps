# Extension VSCode pour APS


Extension permettant d'avoir la coloration syntaxique pour le langage du cours APS (Master STL Sorbonne Université).

# Utilisation

Recopier le dossier `aps` dans `~/.vscode/extensions
